# How to use this tool

Just pass the card name (in portuguese) forward, as in:

```$ npm start Pasmar```

If you have a name with space, single quote it:

```$ npm start 'Anjo Serra'```

This script is not prepared to return more than 1 result, so, if
you have a list of cards, create a file within this same 
directory named list.txt and add all names you want, one per 
line. After that, you can run the command:

```$ npm run list```

# How accurate are the results?

Well, very accurate if you type correctly. I do not have the data
but 1 out of 20 times I run this script I encounter problems.

Generally, problems result from my typos. Other often error 
happen when you have more than 1 result for your card.

You should be fine. Probably.

# What's going on?

This is a very simple tool that access magiccards.info for you,
reads the page and matchs the HTML nodes containing the card's
english name.

So, in theory, this script should work for every non-english
card.

It's important to notice that this operation is VERY similar to
access manually the website magiccards.info and grab the 
information yourself - this is just a more convenient way - and, 
as it is, be aware that the people from magiccards.info can take 
providences against you.

I made this tool because I have a LOT (I really mean A LOT) of 
cards in Portuguese and it really sucks to type it's names every
time I'm build a new deck.