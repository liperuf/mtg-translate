var argv = require('minimist')(process.argv.slice(2));
var cheerio = require('cheerio');
var request = require('sync-request');

var langs = {
  'en': "English",
  'ge': "German",
  'fr': "French",
  'it': "Italian",
  'es': "Spanish",
  'pt': "Portuguese",
  'jp': "Japanese",
  'ch': "Simplified Chinese",
  'ru': "Russian",
};

var debug = argv.o;
var cardName = argv.c || argv.n || argv.card || argv.cardName ||  argv.name || argv._[0];
var cardLangInput = Object.keys(langs).indexOf(argv.i || argv.langIn) >= 0? (argv.i || argv.langIn) : ''; // default ANY
var cardLangOutput = Object.keys(langs).indexOf(argv.o || argv.langOut) >= 0? (argv.o || argv.langOut) : 'en'; // default EN

function parse (response, language) {
  var $ = cheerio.load(response);
  var pattern = 'td[width="30%"] img[class^=flag][alt='+ (langs[language] || langs['en'] || 'English') +'] + a';

  var output = $(pattern).first().text();
  var near = false;

  if(!output) {
    var patternDidYouMean = "form[action='/query'] ~ ul a";
    output = $(patternDidYouMean).first().text();
    near = output? true : false;
  }
  
  return output + (near? ' ~ typo' : '');
}

var query = {}
query.q = cardName + (cardLangInput? ' l:' + cardLangInput : '');

function output(body) {
  return parse(body, cardLangOutput) || cardName + ' * not found';
}

var options = {
  qs: query,
  headers: {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8'
  }
}

var req = request('GET', 'http://magiccards.info/query', options); 

console.log(output(req.getBody()));